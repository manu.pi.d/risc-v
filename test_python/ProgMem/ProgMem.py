import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint


CLK_PERIOD = 10
DATA_WIDTH = 32
ADDR_WIDTH = 10

@cocotb.coroutine
def InitDUT(dut):
    dut.ADDR_i   <= 0
    dut.DATA_o   <= 0
    dut.CLK_i    <= 0
    dut.RESET_i  <= 0

    yield RisingEdge(dut.CLK_i)

    dut.RESET_i  <= 1
    
    yield RisingEdge(dut.CLK_i)
    yield RisingEdge(dut.CLK_i)
    yield RisingEdge(dut.CLK_i)

    dut.RESET_i  <= 0


@cocotb.test()
def test(dut):
    """
    Description:
        Data memory test
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())

    yield InitDUT(dut)

    yield RisingEdge(dut.CLK_i)
    
    for i in range(0,2**ADDR_WIDTH-1):
        dut.ADDR_i <= i
        yield RisingEdge(dut.CLK_i)

    yield RisingEdge(dut.CLK_i)